from celery import Celery

app = Celery('tasks', broker='pyamqp://user:ChangeMe@message-broker//')

@app.task
def add(x, y):
    return x + y