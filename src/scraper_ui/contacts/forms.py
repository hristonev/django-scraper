from django.forms import ModelForm
from .models import WebSite
from .validators import validate_fqdn
from django.core.exceptions import ValidationError


class WebSiteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.owner = None
        self.fields['domain'].validators.append(validate_fqdn)
        self.fields['domain'].validators.append(self.validate_uniq)
        self.fields['domain'].widget.attrs = {
            'class': 'form-control',
            'placeholder': 'Please, enter your domain name'
        }

    class Meta:
        model = WebSite
        fields = ['domain']

    def set_owner(self, owner):
        self.owner = owner

    def validate_uniq(self, domain):
        try:
            WebSite.objects.get(domain=domain, owner=self.owner)
        except WebSite.DoesNotExist:
            pass
        else:
            raise ValidationError('WebSite already exists')

    def clean(self):
        cleaned_data = super().clean()



        return cleaned_data
