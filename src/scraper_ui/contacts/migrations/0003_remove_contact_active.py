# Generated by Django 5.0.2 on 2024-02-11 14:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0002_alter_collector_code_alter_collector_unique_together'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='active',
        ),
    ]
