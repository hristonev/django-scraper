# Generated by Django 5.0.2 on 2024-02-10 16:05

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AlterField(
            model_name='collector',
            name='code',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterUniqueTogether(
            name='collector',
            unique_together={('code', 'owner')},
        ),
    ]
