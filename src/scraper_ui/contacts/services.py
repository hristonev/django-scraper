from scraper.collectors import get_collector_codes


def init_collectors(user):
    from .models import Collector
    for code in get_collector_codes():
        collector = Collector()
        collector.code = code
        collector.owner = user
        collector.active = True
        collector.save()