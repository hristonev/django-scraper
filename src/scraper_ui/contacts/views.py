from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import WebSiteForm
from .models import Collector, Contact
from django.views.generic import ListView


@login_required(login_url='/members/login-user')
def dashboard(request):
    form = WebSiteForm(request.POST or None)
    if request.method == 'POST':
        form.set_owner(request.user)
        if form.is_valid():
            site = form.save(commit=False)
            site.owner = request.user
            site.save()
            messages.add_message(request, messages.INFO, "Website added successfully")
            redirect('dashboard')


    context = {'form': form}
    return render(request, "dashboard.html", context)

def collectors(request):
    own_collectors = Collector.objects.filter(owner=request.user)
    context = {'collectors': own_collectors}
    return render(request, "collectors.html", context)

def collector_toggle(request):
    if request.method == "POST":
        data = request.POST
        collector = Collector.objects.get(owner=request.user, pk=data.get('id'))
        collector.active ^= 1
        collector.save()
        return redirect('collectors')

class ContactListView(ListView):
    paginate_by = 20
    model = Contact
    ordering = ['-id']

    def get_queryset(self):
        return Contact.objects.filter(site__owner=self.request.user)