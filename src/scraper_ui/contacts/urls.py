from django.urls import path
from . import views

urlpatterns = [
    path("", views.dashboard, name="dashboard"),
    path("collectors/", views.collectors, name="collectors"),
    path("collectors/toggle", views.collector_toggle, name="collector_toggle"),
    path("contacts/", views.ContactListView.as_view(), name="contacts")
]
