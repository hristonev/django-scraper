from django.core.exceptions import ValidationError
import re

fqdn = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)


def validate_fqdn(hostname):
    if len(hostname) > 255:
        raise ValidationError('Domain is too long')
    parts = hostname.split(".")
    if len(parts) < 2 or not all(fqdn.match(x) for x in parts):
        raise ValidationError('Invalid domain')
