from django.db import models
from members.models import User
from .validators import validate_fqdn
from django.dispatch import receiver
from django.db.models.signals import post_save
from scraper.services import scrape_web_site


class WebSite(models.Model):
    domain = models.CharField(max_length=255, validators=[validate_fqdn])
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    date_added = models.DateTimeField("date added", auto_now_add=True)

    class Meta:
        unique_together = ('domain', 'owner')


class Collector(models.Model):
    code = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    active = models.BooleanField()

    class Meta:
        unique_together = ('code', 'owner',)


class Contact(models.Model):
    site = models.ForeignKey(WebSite, on_delete=models.CASCADE)
    collector = models.ForeignKey(Collector, on_delete=models.CASCADE)
    type = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    context = models.JSONField()

@receiver(post_save, sender=WebSite)
def website_created(sender, **kwargs):
    if kwargs['created']:
        scrape_web_site(kwargs['instance'])
