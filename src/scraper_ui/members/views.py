from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegisterForm


def register_user(request):
    form = RegisterForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Successful registration")
            return redirect('login')
    context = {'form': form}
    return render(request, 'auth/register.html', context)

def login_user(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "Login success")
            return redirect('dashboard')
        else:
            messages.error(request, "Invalid credentials!")
            return redirect('login')
    elif request.user.is_authenticated:
        return redirect('dashboard')
    else:
        return render(request, 'auth/login.html')

def logout_user(request):
    logout(request)
    return redirect('login')