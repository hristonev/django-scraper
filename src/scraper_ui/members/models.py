from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.db.models.signals import post_save
from contacts.services import init_collectors


class User(AbstractUser):
    USERNAME_FIELD = 'username'
    pass

@receiver(post_save, sender=User)
def user_collectors(sender, **kwargs):
    if kwargs['created']:
        init_collectors(kwargs['instance'])