from threading import Thread
import whois
from .data import CollectorResult

class WhoIsModule(Thread):
    """
    WhoIs module. Works with collectors.
    """
    def __init__(self, data, web_site, collectors):
        super().__init__()
        self.data = data
        self.web_site = web_site
        self.collectors = collectors

    def run(self):
        data = whois.whois(self.web_site)
        for c in self.collectors:
            for value in c.collect(data):
                if len(value) > 5:
                    self.data.put(
                        CollectorResult(
                            value=value,
                            collector=c.get_code()
                        )
                    )