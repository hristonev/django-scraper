import sys, inspect, re


class Collector:
    """
    Base interface. All collectors must implement it.
    """
    def __init__(self):
        pass

    def get_code(self):
        raise NotImplementedError

    def get_type(self):
        raise NotImplementedError

    def collect(self, data):
        raise NotImplementedError


class SpiderInterface(Collector):
    """
    Crawler type of data collection
    """
    def __init__(self):
        super().__init__()

class WhoIsInterface(Collector):
    """
    WhoIs type of data collection
    """
    def __init__(self):
        super().__init__()


class EmailPageContent(SpiderInterface):
    """
    Crawler email collector. Regexp based.
    """
    def __init__(self):
        super().__init__()
        self.pattern = re.compile("""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""")

    def get_code(self):
        return 'scraper.email'

    def get_type(self):
        return 'email'

    def collect(self, data):
        return self.pattern.findall(data['html_doc'])


class PhonePageContent(SpiderInterface):
    """
    Crawler phone number collector. Based on regexp.
    """
    def __init__(self):
        super().__init__()
        self.pattern = re.compile("\\+?\\d{1,4}?[-.\\s]?\\(?\\d{1,3}?\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,9}")

    def get_code(self):
        return 'scraper.phone'

    def get_type(self):
        return 'phone'

    def collect(self, data):
        return self.pattern.findall(data['html_doc'])

class EmailDatabase(WhoIsInterface):
    """
    WhoIs email collector. Fetch list of given emails.
    """
    def __init__(self):
        super().__init__()

    def get_code(self):
        return 'whois.email'

    def get_type(self):
        return 'email'

    def collect(self, data):
        return data['emails']

def get_collector_codes():
    codes = []
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj) and issubclass(obj, Collector):
            try:
                n = globals()[name]
                i = n()
                codes.append(i.get_code())
            except NotImplementedError:
                pass
    return codes

def get_collector_instance(code):
    for name, obj in inspect.getmembers(sys.modules[__name__]):
        if inspect.isclass(obj) and issubclass(obj, Collector):
            try:
                n = globals()[name]
                i = n()
                if code == i.get_code():
                    return i
            except NotImplementedError:
                pass