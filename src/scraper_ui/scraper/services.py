from .tasks import scrape_it

def scrape_web_site(site):
    """
    Check for valid collectors and send request to collector service.
    :param site:
    :return:
    """
    from contacts.models import Collector
    collectors = [c.code for c in Collector.objects.filter(owner=site.owner,active=True)]
    scrape_it.delay(site.id, collectors)