from celery import shared_task
from queue import Queue, Empty
import os
from .scraper_module import ScraperModule
from .whois_module import WhoIsModule
from .collectors import get_collector_instance, SpiderInterface, WhoIsInterface
from .data import CollectorResult

@shared_task
def scrape_it(website_id, collectors):
    """
    Scraper entrypoint. Method routes trough group of collectors.
    New collector engine must be implemented here.
    :param website_id:
    :param collectors:
    :return:
    """
    from contacts.models import WebSite
    website = WebSite.objects.get(pk=website_id)
    collector_groups = {
        SpiderInterface: [],
        WhoIsInterface: []
    }

    for code in collectors:
        collector = get_collector_instance(code)
        for interface in collector_groups:
            if isinstance(collector, interface):
                collector_groups[interface].append(collector)

    collected_data = Queue()

    t = [
        ScraperModule(
            data=collected_data,
            web_site=website.domain,
            collectors=collector_groups[SpiderInterface],
            num_threads=os.environ.get('SPIDER_MAX_THREADS', 2)
        ),
        WhoIsModule(
            data=collected_data,
            web_site=website.domain,
            collectors=collector_groups[WhoIsInterface]
        )
    ]
    for w in t:
        w.start()
    while True:
        try:
            resource = collected_data.get(timeout=1)
            save_data(website, resource)
        except Empty:
            pass
        if not any([w.is_alive() for w in t]):
            break
    for w in t:
        w.join()

def save_data(web_site, resource: CollectorResult):
    """
    Saves scraped data to Django DB
    :param web_site:
    :param resource:
    :return:
    """
    from contacts.models import Contact, Collector, WebSite
    collector = get_collector_instance(resource.collector)
    c = Contact()
    c.type = collector.get_type()
    c.value = resource.value
    c.context = resource.context
    c.collector = Collector.objects.get(code=resource.collector, owner=web_site.owner)
    c.site = WebSite.objects.get(pk=web_site.id)
    c.save()

