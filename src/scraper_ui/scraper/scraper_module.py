from threading import Thread
from queue import Queue, Empty
import urllib.request
import gzip
import brotli
import os
from urllib.parse import urlparse
from .data import CollectorResult
from lxml import html
import random
import time


class ScraperModule(Thread):
    """
    Scraper module. Represent web crawler. Configurable via environment variables.
    TODO Make preflight request to determine scheme and domain (https as default, www?)
    """
    def __init__(self, data, web_site, collectors, num_threads):
        super().__init__()
        self.data = data
        self.incomplete = Queue()
        self.known = set()
        self.web_site = web_site
        self.num_threads = num_threads
        self.collectors = collectors
        self.parsed_links = Queue()

        # Add as first links domain itself
        url = urlparse(web_site)
        if url.scheme:
            l = url.geturl()
            self.incomplete.put()
            self.known.add(l)
        else:
            l = urlparse(f"http://{web_site}").geturl()
            self.known.add(l)
            self.incomplete.put(l)
            l = urlparse(f"https://{web_site}").geturl()
            self.known.add(l)
            self.incomplete.put(l)

    def add_link(self):
        try:
            l = self.parsed_links.get()
            if l not in self.known:
                self.known.add(l)
                self.incomplete.put(l)
        except Empty:
            pass

    def run(self):
        collected = set()
        data = Queue()
        crawlers = [Crawler(domain=self.web_site, data=data, collectors=self.collectors, pending=self.incomplete, new=self.parsed_links) for _ in range(self.num_threads)]
        for c in crawlers:
            c.start()
        while True:
            if not any([w.is_alive() for w in crawlers]):
                break
            try:
                d = data.get(timeout=1)
                key = d.hash()
                if key not in collected:
                    collected.add(key)
                    self.data.put(d)
            except Empty:
                pass
            self.add_link()

class Crawler(Thread):
    def __init__(self, domain, data, collectors, pending: Queue, new: Queue):
        super().__init__()
        self.domain = domain
        self.pending = pending
        self.collectors = collectors
        self.data = data
        self.new = new

        self.request_headers = {
            'User-Agent': os.environ.get('BOT_NAME', 'some-name'),
            'Accept-Encoding': "gzip, deflate, br",
            'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        }

    def __request(self, url):
        self.url = url
        req = urllib.request.Request(url=url, headers=self.request_headers, method='GET')
        with urllib.request.urlopen(req, timeout=300) as response:
            headers = dict(response.info())
            if headers.get('Content-Encoding', '').lower() in ['gzip', 'application/x-gzip']:
                doc = gzip.decompress(response.read())
            elif headers.get('Content-Encoding', '').lower() == 'br':
                doc = brotli.decompress(response.read())
            else:
                try:
                    doc = response.read()
                except UnicodeDecodeError:
                    doc = None
            return headers, doc

    def collect_links(self, html_doc):
        dom = html.fromstring(html_doc)
        for l in dom.xpath("//a/@href"):
            base = urlparse(self.url)
            url = urlparse(l)
            if not url.scheme and not url.netloc:
                l = l.strip("/")
                url = urlparse(f"{base.scheme}://{base.netloc}/{l}")
            elif url.netloc != base.netloc:
                url = None
            if url:
                self.new.put(url.geturl())

    def run(self):
        count = 10
        while True:
            try:
                address = self.pending.get(timeout=1)
                r = self.__request(address)
                if r:
                    try:
                        headers, html_doc = r
                        html_doc = html_doc.decode()
                        data = {
                            'headers': headers,
                            'html_doc': html_doc
                        }
                        for c in self.collectors:
                            for value in c.collect(data):
                                self.data.put(
                                    CollectorResult(
                                        value=value,
                                        collector=c.get_code()
                                    )
                                )
                        self.collect_links(html_doc)
                    except UnicodeDecodeError:
                        pass
            except Empty:
                count -= 1
                if count < 0:
                    break
            random_delay = random.randrange(5, 15) / 10
            time.sleep(os.environ.get('BOT_DELAY', 5) * random_delay)