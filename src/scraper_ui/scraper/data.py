from dataclasses import dataclass, field, asdict
from collections import defaultdict
import json

@dataclass
class CollectorResult:
    """
    Dataclass represents collector result.
    """
    value: str
    collector: str
    context: defaultdict[dict] = field(default_factory=lambda: defaultdict(dict))

    @property
    def __dict__(self):
        return asdict(self)

    def hash(self):
        return hash((self.value, self.collector))