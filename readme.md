# Scraper application
## Components
- Django framework
- Rabbit MQ
- Docker

## Installation
- Install Docker Compose to your host system [external documentation](https://docs.docker.com/compose/install/);
- Run the app via ```docker compose up -d``` in the terminal (Linux/MacOS);

## Application Overview
With your favorite browser navigate to ```http://localhost:8000```. Application interface requires registration.
After successful login you can collect data from websites. In `Collector` section you can activate/deactivate different collectors.
Raw data is presented in `All Contacts`.

## Environment variables
- SPIDER_MAX_THREADS how many threads for a single site crawler.
- BOT_DELAY every iteration worker delay (there is also random multiplier 0.5-1.5)
- BOT_NAME request headers User-Agent

## Debug tools
### Message Broker RabbitMQ
The message broker management interface is exposed on port ```15672```.
Default username ```user```, default password ```ChangeMe```.

## EULA
Please, use this application with consideration of data privacy. Please do not push it to the limits, you probably will be banned in all cases this is not good behavior.