FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
supervisor \
python3.10 \
python3-pip \
curl \
wget \
vim

WORKDIR /app
COPY requirements.txt .
RUN pip install setuptools~=57.5.0
RUN pip install -r requirements.txt
COPY src .
EXPOSE 8000

###### PYCHARM DEPENDENCIES #######

#CMD ["bin/spark-class", "org.apache.spark.deploy.master.Master"]

CMD ["bash", "entrypoint.sh"]